import { useEffect, useState } from 'react'
import './App.css'
import { socket } from './modules/socket'

function App() {
  const [messageList, updateMessageList] = useState<string[]>([])
  const [newMessage, updateNewMessage] = useState<string>('')

  useEffect(() => {
    socket.on('chat message', (msg: string) => {
      console.log('new message received', msg)
      updateMessageList([...messageList, msg])
    })

    socket.on('chouchou', () => {
      updateMessageList([...messageList, 'chouchou'])
    })

    socket.on('boom', () => {
      updateMessageList([...messageList, 'boom'])
    })

    return () => {
      socket.off('chat message')
      socket.off('chouchou')
      socket.off('boom')
    }
  })

  return (
    <div>
      <ul>
        {messageList.map((message, index) => (
          <li key={index}>{message}</li>
        ))}
      </ul>
      <form action="">
        <input
          type="text"
          id="message"
          onChange={(e) => updateNewMessage(e.target.value)}
          value={newMessage}
        />

        <button
          onClick={(e) => {
            e.preventDefault()
            socket.emit('chat message', newMessage)
            updateNewMessage('')
          }}
        >
          Send
        </button>

        <button
          onClick={(e) => {
            e.preventDefault()
            socket.emit('send-others-a-message', newMessage)
            updateNewMessage('')
          }}
        >
          Send to others
        </button>

        <button
          onClick={(e) => {
            e.preventDefault()
            socket.emit('send-to-last-socket', newMessage)
            updateNewMessage('')
          }}
        >
          Send to last socket
        </button>

        <button
          onClick={(e) => {
            e.preventDefault()
            socket.emit("patate-socket")
          }}
        >
          Patate
        </button>

        <button
          onClick={(e) => {
            e.preventDefault()
            socket.emit("bim-socket")
          }}
        >
          Bim
        </button>

      </form>

    </div>
  )
}

export default App
